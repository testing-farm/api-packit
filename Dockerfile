FROM fedora:latest
LABEL maintainer="mvadkert@redhat.com"

# Environment
ENV PYTHONPATH=/opt/scheduler
ENV FLASK_APP=scheduler.py
ENV FLASK_RUN_PORT=8080
ENV OC_VERSION "v3.11.0"
ENV OC_RELEASE "openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit"
ENV HOME=/tmp

# Install pip
RUN dnf -y install python3-pip && dnf clean all && rm -rf /var/cache/dnf/*

# Install scheduler
RUN mkdir -p /opt/scheduler
COPY requirements.txt /opt/scheduler/requirements.txt
RUN pip3 install --no-cache-dir -r /opt/scheduler/requirements.txt

# Deploy application
# COPY gunicorn.py /opt/scheduler/gunicorn.py
COPY scheduler.py /opt/scheduler/scheduler.py
WORKDIR /opt/scheduler

# install the oc client tools
ADD https://github.com/openshift/origin/releases/download/$OC_VERSION/$OC_RELEASE.tar.gz /opt/oc/release.tar.gz
RUN tar --strip-components=1 -xzvf  /opt/oc/release.tar.gz -C /opt/oc/ && \
    mv /opt/oc/oc /usr/bin/ && \
    rm -rf /opt/oc

EXPOSE 8080

CMD ["flask", "run", "-h", "0.0.0.0"]
