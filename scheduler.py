import configparser
import os
import logging
import socket
import pprint

from flask import jsonify, request, Flask, Response, abort
from flask_cors import CORS
from subprocess import check_call, check_output, Popen, PIPE, STDOUT

VERSION="0.0.1"
DEFAULT_PORT=8080
CONFIG="config/scheduler"
POD="""
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: {pod_name}
  name: {pod_name}
spec:
  securityContext:
    runAsUser: 0
  nodeSelector:
    oci_kvm_hook: allowed
  serviceAccount: testing-farm
  containers:
  - args:
    - cruncher
    - --pipeline-id={pipeline_id}
    - --git-url={git_url}
    - --git-ref={git_ref}
    - --git-commit-sha={commit_sha}
    - --git-repo-name={repo_name}
    - --git-repo-namespace={repo_namespace}
    - --copr-chroot={copr_chroot}
    - --copr-name={copr_repo_name}
    - --post-results
    - --post-results-url={post_results_url}
    - --post-results-token={post_results_token}
    - --artifacts-dir={pipeline_artifacts_dir}
    image: quay.io/testing-farm/cruncher
    imagePullPolicy: Always
    name: {pod_name}
    volumeMounts:
      - mountPath: {artifacts_dir}
        name: volume-artifacts
    resources: {{}}
  volumes:
    - name: volume-artifacts
      persistentVolumeClaim:
        claimName: testing-farm-artifacts
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {{}}
"""

# initialize Flask with cors
scheduler = Flask(__name__)
CORS(scheduler, resources={
  r'/v0/console/.*': {
    'origins': ['http://localhost:3000', 'https://console-testing-farm.apps.ci.centos.org']
  }
})

# initialize logger
log = scheduler.logger

# set logging to info
logging.basicConfig(level=logging.INFO)

# login to openshift
config = configparser.ConfigParser()
config.read(CONFIG)

check_call(['oc', 'login', config['openshift']['url'], '--insecure-skip-tls-verify=true', '--token', config['openshift']['token']])
check_call(['oc', 'project', config['openshift']['project']])

token = config['api']['token']

@scheduler.route('/v0/version')
def version():
    return jsonify(
        {
            'name': 'Testing Farm API',
            'version': VERSION,
        }
    )


@scheduler.route('/v0/result', methods=['POST'])
def results():
    json = request.get_json()

    log.info('received result:\n{}'.format(pprint.pformat(json)))

    return jsonify(success=True)

def stream(generator):
  for line in generator:
    yield line


@scheduler.route('/v0/console/<id>', methods=['GET'])
def console(id):
    command = ['oc', 'logs', 'pod/tft-{}'.format(id)]

    process = Popen(command, stdout=PIPE, stderr=STDOUT)

    generator = stream(iter(process.stdout.readline, b''))

    return Response(generator, mimetype='text/plain')


@scheduler.route('/v0/trigger', methods=['POST'])
def trigger():
    req = request.get_json()

    if not req:
        log.error("Could not get json in request.")
        abort(405)

    log.info('received trigger:\n{}'.format(pprint.pformat(req)))

    api = req.get('api')

    if not api or not api['token']:
        log.error("Required array 'api' with key 'token' not found.")
        abort(405)

    if api['token'] != token:
        log.error("Invalid API token.")
        abort(401)

    pipeline = req.get('pipeline')

    if not pipeline or not pipeline['id']:
        log.error("Required array 'pipeline' with key 'id' not found.")
        abort(405)

    artifact = req.get('artifact')

    for key in ['repo-name', 'repo-namespace', 'copr-repo-name', 'copr-chroot', 'commit-sha', 'git-url', 'git-ref']:
        value = artifact.get(key, None)
        if not value:
          log.error("Required key '{}' in array 'artifact' not found.".format(key))
          abort(405)

    # We send results
    post_results_url = [config['url']['tft-results']]

    # Support post urls from Packit: https://github.com/packit-service/packit-service/pull/499
    # Default to the production instance for now
    if req.get('response-url'):
        post_results_url.append(req.get('response-url'))
    else:
        post_results_url.append(config['url']['packit-results'])

    pod = POD.format(
      pod_name='tft-{id}'.format(id=pipeline['id']),
      pipeline_id=pipeline['id'],
      git_url=artifact['git-url'],
      git_ref=artifact['git-ref'],
      commit_sha=artifact['commit-sha'],
      repo_name=artifact['repo-name'],
      repo_namespace=artifact['repo-namespace'],
      copr_chroot=artifact['copr-chroot'],
      copr_repo_name=artifact['copr-repo-name'],
      pipeline_artifacts_dir=os.path.join(config['cruncher']['artifacts-dir'], pipeline['id']),
      artifacts_dir=config['cruncher']['artifacts-dir'],
      # we use the same secret here
      post_results_token=config['api']['token'],
      post_results_url=','.join(post_results_url)
    )

    log.info('submitting pod config:\n{}'.format(pod))

    pod = bytes(pod, 'utf-8')

    process = Popen(["oc", "create", "-f", "-"], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    stdout, stderr = process.communicate(input=pod)

    log.info('oc create stdout:\n{}'.format(stdout))
    log.info('oc create stderr:\n{}'.format(stderr))

    if process.returncode != 0:
      log.error('Could not schedule testing: {}'.format(stderr))
      abort(405)

    return jsonify(success=True, id=pipeline['id'], url='{}/pipeline/{}'.format(config['api']['console-url'], pipeline['id']))

if __name__ == '__main__':
    scheduler.run(host='0.0.0.0', port=DEFAULT_PORT)
